#Test Assignment 

I will able to deliver this project by 15/10/17.

Migration from Spring JDBC to Spring + JPA (Hibernate)

Integration of Data Scrubbing and log functionality

We had migrated project into JPA so you need to do following thing after taking clone.

1) Encrypt the password using "com.gjj.igden.service.passwordencoder.AppPasswordEncoder" file  main method, already one example is given.
2) Need to make one entry in "app_role" table for Role i.e. code="ROLE_ADMIN" and name="ROLE_ADMIN"
3) Need to make entry in "account_roles" table for the existing user. In account_roles tables, account_id is account table primary key and role value should be code of "app_role" table. 

